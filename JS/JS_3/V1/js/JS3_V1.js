$(document).ready(function () {

    $("#textIMC").append("<span>" + calculerIMC() + "</span>")

    let poids = $("#idPoids").val();
    let taille = $("#idTaille").val();

    poids = Number(poids);
    taille = Number(taille);

    taille = taille / 100 ;

    calculerIMC(poids, taille);

   // imc.toFixed(1) ;

    function calculerIMC(prmPoids, prmTaille) {

        let valRetour = prmPoids / (prmTaille * prmTaille);

        valRetour.toFixed(1) ;

        return valRetour;
    }

})