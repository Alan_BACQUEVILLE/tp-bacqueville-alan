let taux = 1.65 ;                                                                       //taux du CAD
let resultfonc = "" ;


resultfonc = convEuDo(taux) ;                                                           //Utilisation de la fonction

alert(resultfonc) ;                                                                     //Affichage du message en alerte


function convEuDo(prmTaux){                                                             //Déclaration fonction
    let euros = 16384 ;                                                                  //Valeurs en euro
    let resultat = 0 ;                                                                   //Variable resultat
    let msg = "" ;                                                                       //Variable pour le message
    let someuros = 1 ;                                                                   //Pour la multiplication

    for(let i = 1 ; i <= 15 ; i++){                                                     //Pour i allalt de 0  à euros
        resultat = someuros * prmTaux ;                                                 //conversion en CAD
        resultat = resultat.toFixed(2);                                                 //limitation à 2 chiffres après la virgules
        msg = msg + someuros + "euro(s) = " + resultat + " dollar(s) \n" ;              //Paramétrage du message 
        someuros = someuros * 2 ;                                                     
    }
    return msg ;
}