let multi = 7 ;                                                     //multiplicateur
let nbfois = 20 ;                                                   //nombre de multiplication
let resultat ;                                                      //resultat après calcul
let msgvaleur = "" ;                                                //Sauvegarde des précédent résultat

msgvaleur = multiplication(multi,nbfois) ;                          //Utiisation de la fonction 

alert(msgvaleur ) ;                                                 //Affichage des valeurs et du résultat)

function multiplication(prmMulti, prmNbfois){                       //Déclaration de la fonction 
    let result = 0 ;
    let msgaff = "" ;
    for(let i = 0 ; i < prmNbfois ; i++){                         //pour nbfois on fait 
        result = i * prmMulti ;                                      //Valeur de i * le multiplicateur 
        msgaff = msgaff + " / " + result                                //On ajoute le résultat obtenu a la variable de sauvegarde
        //i = i + 1 ;                                                 //On ajoute 1 à i pour le prochain calcul
    }
    return msgaff ;
}