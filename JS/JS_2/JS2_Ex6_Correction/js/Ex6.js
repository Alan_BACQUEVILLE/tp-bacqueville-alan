$(document).ready(function () {

    /* Gestionnaire d'évènement CLICK sur le bouton "Ajouter" */ 
    $("#btnAjout").click(function() {
        //ajout d'une ligne à la fin de la partie TBODY
        $("#tab1 tbody").append("<tr><td>1</td></tr>");
    });

    /* Gestionnaire d'évènement CLICK sur le bouton "Supprimer" */ 
    $("#btnSuppr").click(function() {
        //suppression de la dernière ligne de la partie TBODY
        $("#tab1 tbody tr:last").remove();
    });

});