$(document).ready (function(){              //Attendre le chargement du DOM

    $("#btn1").click(affTxt) ;              //Si clicque sur btn1 exécuté la fonction 

    $("#btn1").dblclick(effTxt) ;

    function affTxt(){                      //Déclaration de la fonction 
      $("#maDiv1").html("Mon texte affiché grâce à jQuery") ;     //la focntion affiche le message souhaité
      $("#btn1").attr("value", "Fermer") ;                        //Modification de la valeur du bouton
    }

    function effTxt(){
      $("#maDiv1").html("") ;                      //la fonction ferme le message
      $("#btn1").attr("value", "ouvrir") ;         //Modification de la valeur du bouton
    }

})