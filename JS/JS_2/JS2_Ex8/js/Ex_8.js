$(document).ready(function(){
    
    $("#btn1").click(function(){
        ajouterLigne();
        nblongeurtab = $("#maTable tbody tr").length ;
        let addtotal = calculTotal(nblongeurtab);
        $("#maTable tfoot td").html(addtotal) ;
    });
    
    
    
    $("#btn2").click(function(){
        supprimerLigne();
        let nblongeurtab = $("#maTable tbody tr").length ;
        let addtotal = calculTotal(nblongeurtab);
        $("#maTable tfoot td").html(addtotal) ;
    });
   
    
    
    function ajouterLigne(){
        let tailletableau = $("#maTable tbody tr").length ;
        $("#maTable tbody").append("<tr><td>" + tailletableau + "</td></tr>") ;
    }
    
    function supprimerLigne(){
        $("#maTable tbody tr:last").remove() ;       
    }
    
    function calculTotal(prmlongeurtab){
        let resultTotal = 0 ;
        for(let i = 0 ; i < prmlongeurtab ; i++){
            resultTotal = resultTotal + i ;
        }
        return resultTotal ;
    }
});