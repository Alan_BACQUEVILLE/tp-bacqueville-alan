$(document).ready(function () {                             //Attendre que la page html soit initilisé

    $("#photo").click(remplacement);                        //Appel de la fonction

    function remplacement(ev) {                             //Initialisation de la fonction
        ev.preventDefault();                                //désactiver le comportement normal d'un lien
        $("#photo").replaceWith("<img src='img/photoRemplacement.jpg' alt=''>");    //Remplacement du lien par l'image

    }

});