let n = prompt ("n = ")  ;                              //Déclaration et initialisation de la variable 
n = parseInt(n) ;                                       //Passage en nombre entier
let resultat = 1 ;                                      //Déclaration et initialisation de la variable
let i = 0 ;                                             //Déclaration et initialisation de la variable
let res = 0 ;                                           //Déclaration et initialisation de la variable

res = factorielle(n);                                   //Stockage de la valeur de la fonction dans une variable 

alert("La fonction factorielle est égale à " + res);    //Affichage du résultat de la fonction par une alerte

function factorielle(n){                                //Déclaration de la fonction 
    for(i = 1 ; i <= n ; i++){                          //Boucle pour 
        resultat = resultat * i ;                       //Calcul 
    }
    return resultat ;                                   //Retour de la fonction 
}