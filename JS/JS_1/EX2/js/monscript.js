let op1 = prompt("Premiere opérande = ");               //Déclaration du premier nombre
let op2 = prompt("Deuxième opérande = ");               //Déclaration du scond nombre


let op1PasNombre = isNaN(op1);          //Vérification pour savoir si op1 est un nombre
let op2PasNombre = isNaN(op2);          //Vérification pour savoir si op2 est un nombre

if (op1PasNombre || op2PasNombre) {     //Si op1 et op2 ne sont pas des nombres alors
    alert("Un des deux opérandes n'est pas un nombre");     //Afficher un message d'alerte
} else {                                //Sinon
    if (op2 != 0) {                     //Si op2 différent de 0

        op1 = parseFloat(op1);          //Mise en entier de op1
        op2 = parseFloat(op2);          //Mise en entier de op2
        let resultat = op1 / op2;       //Calcul de la division
        alert(op1 + "/" + op2 + "=" + resultat);            //Afficher le résultat par un mesage d'alerte


    } else {                            //Sinon 
        alert("Division par zéro impossible");              //Afficher un message d'alerte
    }
}


